from django.utils.translation import ugettext as _
from django.http import HttpResponse
def hello(request):
    output = _("Hello World!")
    return HttpResponse(output)